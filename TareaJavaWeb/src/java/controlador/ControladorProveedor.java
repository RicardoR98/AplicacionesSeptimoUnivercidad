package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.ModeloProveedores;
import modelo.ProveedoresDAO;
import singleton.Singleton;

@WebServlet(name = "proveedor.exe", urlPatterns = {"/proveedor.exe"})
public class ControladorProveedor extends HttpServlet {

    private ModeloProveedores modelo;
    private PrintWriter out;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        out = resp.getWriter();

        try {
            modelo = new ModeloProveedores();
            ProveedoresDAO p = new ProveedoresDAO(Singleton.crearConexion());
            modelo.setNombre(req.getParameter("nombre"));
            modelo.setDireccion(req.getParameter("direccion"));
            modelo.setTelefono(req.getParameter("telefono"));
            p.add(modelo);
            out.print("<script>alert('Datos Alacenados');</script>");
            resp.sendRedirect("index.jsp");
        } catch (ClassNotFoundException ex) {
            System.out.println("Error Class: " + ex);
        } catch (SQLException ex) {
            System.out.println("Error SQL: " + ex);
        }

    }

}
