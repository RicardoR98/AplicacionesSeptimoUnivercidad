/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diagnostico;

/**
 *
 * @author rodri
 */
public class Ponentes extends Persona{ //clase heredando de la clase padre Persona
    //Declaracion de variables
    private String temaHabla;
    private String hora;

    //Declaracion de constructor para llenado de datos
    public Ponentes(String nombre, String aMaterno, String aPaterno, String temaHabla, String hora) {
        super(nombre, aMaterno, aPaterno);
        this.temaHabla = temaHabla;
        this.hora = hora;
    }

    //Get y Set de cada uno de los atributos de la clase
    public String getTemaHabla() {
        return temaHabla;
    }

    public void setTemaHabla(String temaHabla) {
        this.temaHabla = temaHabla;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    @Override
    public String toString() {
        return "Tema Habla= " + temaHabla + ", Hora= " + hora;
    }
    
}
