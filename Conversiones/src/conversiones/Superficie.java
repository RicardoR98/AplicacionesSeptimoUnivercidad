/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversiones;

import java.util.Scanner;

/**
 *
 * @author rodri
 */
public class Superficie {

    //Se inicializa la instancia para poder meter datos por medio del teclado de manera global
    Scanner s = new Scanner(System.in);
    
    //Constructor que manda a llamar al metodo menu
    public Superficie() {
        menu();
    }

    //Menu donde se le preguntara al usuario que conversion decea realizar
    private void menu() {
        System.out.println("¿Que desea hacer?");
        System.out.println("*****************");
        System.out.println("1. Metro^2 a Milla^2");
        System.out.println("1. Milla^2 a Metro^2");
        System.out.print("Seleccione la opción deseada: "); int seleccion = s.nextInt();
        switch(seleccion){
            case 1:
                metroMilla();
                break;
            case 2:
                millaMetro();
                break;
            default:
                System.out.println("La opcion es incorrecta");
                menu();
                break;
    
    }
    }

    //Convertir metros cuadrados a millas cuadradas
    private void metroMilla() {
        System.out.print("Ingrese la cantidad: ");double metro = s.nextDouble();
     
        double res = metro * 3.86e-7;
        System.out.println("Tu resultado es: " + res + " millas cuadradas");
    }

    //Convertir de millas cuadradas a metros cuadrados
    private void millaMetro() {
        System.out.print("Ingrese la cantidad: ");double milla = s.nextDouble();
     
          double res = milla * 2589988;
        System.out.println("Tu resultado es: " + res + " metros cuadrados");
    }
    
}
