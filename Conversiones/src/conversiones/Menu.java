/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversiones;

import java.util.Scanner;

/**
 *
 * @author rodri
 */
public class Menu {

    //Constructor que manda a llamar al metodo menu
    public Menu() {
        menu();
    }

    private void temperatura() {
        Temperatura t = new Temperatura();
    }

    private void moneda() {
        Moneda m = new Moneda();
    }

    private void longitud() {
        Longitud l = new Longitud();
    }

    private void superficie() {
        Superficie s = new Superficie();
    }

    //Menu donde se le preguntara al usuario que conversion decea realizar
    private void menu() {
        Scanner s = new Scanner(System.in);
        System.out.println("Bienvenido usuario...");
        System.out.println("¿Que desea hacer?");
        System.out.println("*********************");
        System.out.println("1. Temperatura");
        System.out.println("2. Moneda");
        System.out.println("3. Longitud");
        System.out.println("4. Superficie");
        System.out.print("Seleccione numero: ");int seleccion = s.nextInt();
        //Switch que decidira a donde mandar al usuario segun su seleccion
        switch(seleccion){
            case 1:
                temperatura();
                break;
            case 2:
                moneda();
                break;
            case 3:
                longitud();
                break;
            case 4:
                superficie();
                break;
            default:
                System.out.println("Opcion incorrecta...");
                menu();
                break;
        }
    }
    
}
