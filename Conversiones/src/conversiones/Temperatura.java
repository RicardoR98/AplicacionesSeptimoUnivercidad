/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversiones;

import java.util.Scanner;

/**
 *
 * @author rodri
 */
public class Temperatura {
    
    //Se inicializa la instancia para poder meter datos por medio del teclado de manera global
    Scanner s = new Scanner(System.in);

    //Constructor que manda a llamar al metodo menu
    public Temperatura() {
        menu();
    }

    //Metodo donde convertimos celsius a farenheit
    private void celsiusFarenheit() {
        System.out.print("Ingrese grados celsius: ");
        double celsius = s.nextDouble();

        
        double res = 1.8*celsius+32;
        System.out.println("El resultado es: "+res+" grados farenheit");
    }
    
    //Metodo donde convertimos farenheit a celsius
    private void farenheitCelsius(){
                System.out.print("Ingrese grados farenheit: ");
        double faren = s.nextDouble();
        
        double res = (faren-32)/1.8000;
        System.out.println("El resultado es: "+res+" grados celsius");
    }

    //Menu donde se le preguntara al usuario que conversion decea realizar
    private void menu() {
                System.out.println("¿Que desea hacer?");
        System.out.println("*****************");
        System.out.println("1. Convertir a celsius afarenheit");
        System.out.println("2. Convertir a farenheit a celsius");
        System.out.print("Seleccione la opción deseada: "); int seleccion = s.nextInt();
        switch(seleccion){
            case 1:
                celsiusFarenheit();
                break;
            case 2:
                farenheitCelsius();
                break;
            default:
                System.out.println("La opcion es incorrecta");
                menu();
                break;
        }
    }
    
}
