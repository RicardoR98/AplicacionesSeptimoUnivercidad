/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conversiones;

import java.util.Scanner;

/**
 *
 * @author rodri
 */
public class Conversiones {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Llmado al metodo ciclo desde main
        ciclo();
    }

    private static void ciclo() {
        //Se inicializa la instancia para poder meter datos por medio del teclado
        Scanner s = new Scanner(System.in);
         
        //Variable que guardara la respuesta para ciclar la app
        int res;
        
        try {
                // DoWhile donde ciclaremos la app si es que el usuario desea realizar otra convercion
            do{
                //Intsnaciar al constructor de la clase Menu
                Menu m = new Menu();

                System.out.println("¿Decea otra conversion?");
                System.out.println("1.SI");
                System.out.println("2. NO");
                System.out.println("Seleccion: ");res = s.nextInt();
                if (res != 1 || res != 2) {
                    ciclo();
                }
            }while(res == 1);
        } catch (Exception e) {
            System.out.println("Error solo se aceptan numeros");
        }
    }
}
