package Banco;

public class CuentaOro extends Cuenta{

    //Declaracion de variables de clase
    private double interes = 0.015;
    private String regalo = "Seguro";

  public CuentaOro() {
  }

  //Inicialisacion de variables
  public CuentaOro(int edad, String nombre, String direccion, boolean nomina, boolean pension, double valor) {
    super(edad, nombre, direccion, nomina, pension, valor);
  }    

  public String getRegalo() {
    return regalo;
  }

  public double getInteres() {
    return interes * 100;
  }

  public double cotizar(double valor) {
    
    valor += valor * interes;  
    
    return valor;

  }
  
}
